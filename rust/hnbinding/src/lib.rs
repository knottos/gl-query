use pyo3::prelude::*;
use rand::thread_rng;
use std::collections::HashMap;
use serde::{Serialize, Deserialize};
use std::fs;
use rusqlite::{Connection, Result};

use hypernonsense::multiindex::{ MultiIndex, DistanceNode };
use hypernonsense::vector::{ random_unit_vector, modified_cosine_distance, euclidean_distance };

#[pyclass]
pub struct PyMultiIndex {
    pub index: MultiIndex<isize>,
    pub sqlite_path: String,
}

#[pymethods]
impl PyMultiIndex {
    #[new]
    pub fn new(dimension: usize, indices: u8, planes: u8, sqlite_path: &str) -> PyMultiIndex {
        PyMultiIndex {
            index: MultiIndex::new(dimension, indices, planes, &mut thread_rng()),
            sqlite_path: sqlite_path.to_string(),
        }
    }
    pub fn add(&mut self, key: isize, vector: Vec<f32>) {
        self.index.add(key, &vector);
    }
    pub fn add_from_table(&mut self) {
        let conn = Connection::open(self.sqlite_path.clone()).unwrap();
        let mut stmt = conn.prepare("SELECT id,Embedding FROM Data").unwrap();
        let mut rows = stmt.query([]).unwrap();

        while let Some(row) = rows.next().unwrap() {
            let id: isize = row.get(0).unwrap();
            let embed: String = row.get(1).unwrap();
            let datas: Vec<f32> = serde_json::from_str(&embed).unwrap();
            self.index.add(id, &datas);
        }
    }
    pub fn nearest(&self, vector: Vec<f32>, nearest_count: usize) -> Vec<isize> {
        let indexes = self.index.nearest_points(&vector);
        
        let conn = Connection::open(self.sqlite_path.clone()).unwrap();
        // Ok, lets just try and construct our query in a string :mad_face:
        let values = indexes
            .iter()
            .map(|i| i.to_string())
            .collect::<Vec<_>>()
            .join(",");

        let mut stmt = conn.prepare(&format!("SELECT id,Embedding FROM DATA WHERE id IN ({})", values)).unwrap();
        let mut result = stmt
            .query_map([], |row| {
                let id: isize = row.get(0).unwrap();
                let embed: String = row.get(1).unwrap();
                let datas: Vec<f32> = serde_json::from_str(&embed).unwrap();
                
                Ok((id, datas))
            })
            .unwrap()
            .map(|a| a.unwrap())
            .map(|a| DistanceNode { distance: euclidean_distance(&vector, &a.1), key: a.0 })
            .collect::<Vec<_>>();

        result.sort_unstable();
        result.truncate(nearest_count);
        result.shrink_to_fit();
        
        result
            .into_iter()
            .map(|a| a.key)
            .collect::<Vec<_>>()
        
        //return vec![*x.last().unwrap(), *x.get(0).unwrap()];
    }
}

#[pymodule]
fn _hnbinding(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_class::<PyMultiIndex>()?;
    Ok(())
}