from setuptools import setup
from setuptools_rust import Binding, RustExtension

public = True

if public:
    version = "0.1.1"

setup(
    name="hnbinding",
    version=version,
    rust_extensions=[
        RustExtension(
            "hnbinding._hnbinding",
            binding=Binding.PyO3,
            # Between our use of editable installs and wanting to use Rust for performance sensitive
            # code, it makes sense to just always use --release
            debug=False,
        )
    ],
    package_data={"hnbinding": ["py.typed"]},
    packages=["hnbinding"],
    zip_safe=False,
)