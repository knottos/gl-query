from hnbinding import _hnbinding

def PyMultiIndex(dimension, indices, planes, sqlite_path):
    return _hnbinding.PyMultiIndex(
        dimension=dimension,
        indices=indices,
        planes=planes,
        sqlite_path=sqlite_path,
    )