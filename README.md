# gl-query

Elusive and forbidden knowledge, delivered straight to your application!

## Examples
```
# Boot the server
$ OPENAI_API_TOKEN=<your-token-here> cd app/ && python gl-query-server.py

# In a new terminal
$ gl-query "Who is the CEO of GitLab?"  
Sid Sijbrandij is the Co-founder, Chief Executive Officer and Board Chair of GitLab Inc

$ gl-query "Can I Expense a Desk?"
Yes, you can expense a desk. According to the provided context, the guideline price for a desk is between $300 and $500 USD.

$ gl-query "Prometheus is down, what severity of incident is that? Explain why"
This is a Critical Incident with a High Impact. Prometheus is a key component of our monitoring and alerting system, and its down time can lead to service disruptions and outages. Therefore, it is important to activate Pager Duty immediately.

# More info generally gives better answers:
$ gl-query "Why do we use Ruby?"
We use Ruby because it is widely adopted in the security industry and has a large selection of libraries that can help us quickly develop tools.

$ gl-query "Why do we use Ruby, and not Python?"
We use Python for the majority of our backend development, but we use Ruby for direct quotations from interviewees in blog posts and for instrumenting code to measure performance. We also use it for generating sample data and for automating code quality and testing.

# Shouldnt answer things we dont know about!
$ gl-query "What is the policy for Womble invasions?"
I don't know.
```


## Why?
The handbook is *big*, and while searching or grepping it is fine, having a nice tool to ask it human questions is very nice 

## How?
It uses a openai's embedding API, with a healthy dash of prompt engineering, the results from the handbook are cleaned/ pre-processed, then fed to openai to get embeddings.. results into a useable embeddings.db, which the server loads. 

You can see how this is done in `app/generate_training_data.py` and `app/embed.py`

The heavy processing is done by [Hypernonsense](https://crates.io/crates/hypernonsense), which is an excellent, super speedy vector search library for Rust. There are python bindings in this repo under rust/hnbinding/ for the curious!

## Limitations

- Its not very good with code (formatting, mostly!)
```bash
$ gl-query "What is vault? How do I access it?"

Vault is an identity-based secret and encryption management system. It can be used as a central store to manage access and secrets across applications, systems, and infrastructure. To access Vault, you can use the OIDC authentication method, a SOCKS5 proxy, port-forwarding via `kubectl`, or the `vault-proxy` function for fish shell

$ gl-query "Show me the vault-proxy function for fish shell"

function vault-proxy -d 'Set up a proxy to run vault commands';  set -f BASTION_HOST lb-bastion.ops.gitlab.com;  set -Ux VAULT_ADDR https://vault.ops.gke.gitlab.net;  set -f VAULT_PROXY_PORT 18200;  set -Ux VAULT_PROXY_ADDR socks5://localhost:$VAULT_PROXY_PORT;  set msg [vault] Starting SOCKS5 proxy on $BASTION_HOST via $VAULT_PROXY_ADDR;  if test -n $TMUX;  tmux split-pane -d -v -l 3 echo \$msg\; ssh -D \$VAULT_PROXY_PORT\ \$BASTION_HOST\ 'echo \Connected! Press Enter to disconnect.\; read disconnect'; set -e VAULT_PROXY_ADDR VAULT_ADDR; echo Disconnected ; sleep 3;  else;  echo >&2 Open a new shell before using Vault:;  echo >&2 $msg;  ssh -D $VAULT_PROXY_PORT $BASTION_HOST 'echo Connected! Press Enter to disconnect.; read disconnect' >&2;  set -e VAULT
```
- There are none, it has become more powerful than we could've ever imagined.

## Ok great, how do I install it ?

If you are on nix, the following derivation should do the trick:
```
# something like this, although it probably wont work without the
# embedding.db, which is too big for me to put in this git repo - 
# im considering various means to distribute it

environment.systemPackages = [
  (callPackage (fetchGit https://gitlab.com/knottos/gl-query) {})
];
```

## But im not using nix :(
Sorry, I dont actually know much of anything about python packaging :(, if you'd like to submit a MR, it would be very welcome (please open one aganist main), the list of dependencies is in pkgs.nix. Container image coming soon(tm).