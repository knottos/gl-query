{
  pkgs ? import (fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/e285dd0ca97.tar.gz";
    sha256 = "sha256:09kzsi7ivvq4cziyvar6bppmm6sx9i7w5b4ha2k94w6yns37nr0d";
  }) { config.allowUnfree = true; }
}:
pkgs.stdenv.mkDerivation rec {
  version = "0.0.1";
  name = "gl-query-${version}";
  src = ./.;
  unPackPhase = '''';
  buildPhase = ''
    # Nope
  '';
  installPhase = ''
    mkdir -p $out/bin $out/data
    cp app/gl-query $out/bin/gl-query
    cp app/gl-query-server $out/bin/gl-query-server
    cp app/utils.py $out/bin/utils.py
    
    # cp shell.nix $out/bin/shell.nix
    # cp data/embeddings.csv $out/data/embeddings.csv
  '';
  postInstall = ''
  '';
  buildInputs = [
    (pkgs.callPackage ./pkgs.nix {
      pkgs = pkgs;
    })
  ];
  meta = {
    homepage = http://gitlab.com/knottos/gl-query;
    description = "Query the gitlab handbook from your terminal!";
  };
}