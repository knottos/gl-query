{
  description = "Elusive and forbidden knowledge!";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/f28c957a927b0d23636123850f7ec15bda9aa2f4";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = {self, nixpkgs, flake-utils, ...}: 
    flake-utils.lib.eachDefaultSystem (system:
      let 
        pkgs = import nixpkgs { inherit system; };
        
        glqueryServer = (pkgs.callPackage ./default.nix {});
        dockerImage  = pkgs.dockerTools.buildImage {
          name = "gl-query";
          config = {
            Cmd = [ "${glqueryServer}/bin/gl-query-server" ];
          };
        };
      in {
        packages = {
          docker = dockerImage;
          server = glqueryServer;
        };
        defaultPackage = glqueryServer;
        devShell = (pkgs.callPackage ./shell.nix {});
      });
}