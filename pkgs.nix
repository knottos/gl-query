{ pkgs }:
let
  python-packages = p: with p; [
    openai
    setuptools
    numpy
    matplotlib
    plotly
    scipy
    scikit-learn
    beautifulsoup4
    mistletoe
    flask
    ujson
    (callPackage ./hnbinding.nix {})

    # For CUDA, we've disabled it for now
    # cupy
    # For panda's parallel processing
    (
      buildPythonPackage rec {
        pname = "tiktoken";
        version = "0.1.2";
        src = pkgs.fetchFromGitHub {
            owner = "openai";
            repo = "tiktoken";
            rev = "156eff92d2dc88e108bdece47e584b05859af557";
            hash = "sha256-3vZvibhIEhcNfLIt1yVcjzgwhXMkR0wWcIa0NcPAOxg=";
          };

        cargoDeps = pkgs.rustPlatform.importCargoLock {
          lockFile = ./rust/tiktoken/Cargo.lock;
        };
        patchPhase = ''
          #cargo generate-lockfile
        '';
        propagatedBuildInputs = [
          requests
          regex
          (buildPythonPackage rec {
            pname = "blobfile";
            version = "2.0.1";
            src = pkgs.fetchFromGitHub {
              owner = "christopher-hesse";
              repo  = "blobfile";
              rev   = "d099063d0f732eddb63aa8a22dcfab4d605a99ea";
              hash  = "sha256-EUU/ZORqri3EIUUAho6cNdRgH+we13AgHyTjsY6tCWg=";
            };
            propagatedBuildInputs = [ urllib3 filelock pycryptodomex lxml ];
            doCheck = false;
            }
          )
        ];
        nativeBuildInputs = [ 
          setuptools-rust
        ] ++ (with pkgs.rustPlatform; [
          rust.cargo
          rust.rustc
          cargoSetupHook
        ]);
        doCheck = false;
      }
    )
  ];
in pkgs.python3.withPackages python-packages