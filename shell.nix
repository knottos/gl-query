# shell.nix
{ pkgs ? import <nixpkgs> {
  config.allowUnfree = true;
} }:
let
  python-pkgs = (pkgs.callPackage ./pkgs.nix {
    pkgs = pkgs;
  });
  python-sh-pkgs = p: with p; [
    setuptools
    beautifulsoup4
    mistletoe
  ];
in pkgs.mkShell {
  name = "gl-query";
  packages = [
    python-pkgs
    (pkgs.python3.withPackages python-sh-pkgs)
  ];
  shellHook = ''
      # export CUDA_PATH=${pkgs.cudatoolkit}
      # export LD_LIBRARY_PATH=${pkgs.linuxPackages.nvidia_x11}/lib:${pkgs.ncurses5}/lib
      # export EXTRA_LDFLAGS="-L/lib -L${pkgs.linuxPackages.nvidia_x11}/lib"
      # export EXTRA_CCFLAGS="-I/usr/include"
  ''; 
} 