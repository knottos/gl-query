import os
import openai
import re
import glob
import csv
from itertools import islice
from bs4 import BeautifulSoup, Tag
import mistletoe
import sqlite3
from mistletoe import Document, HTMLRenderer
from itertools import filterfalse as ifilterfalse

curly_tags = re.compile(r"{:.+?}", re.IGNORECASE)

def clean_text(t):
    return curly_tags.sub("", t).strip().replace('"', '')

def sections_content(all_text: str, headers: list[str]) -> list[str]:
    lines = all_text.split('\n')
    heading = ""
    content = ""
    def is_header(ln):
        for h in headers:
            if ln == h:
                return True
        return False

    ret = []
    for idx, line in enumerate(lines):
        # Start looking for a heading.
        if idx + 1 == len(lines) and heading and len(content) > 0:
            #yield [heading, content]
            content = ""
        
        head = is_header(line)
        if head and line.upper() != "ON THIS PAGE" and line.upper() != "TABLE OF CONTENTS":
            if len(content) > 50: # 150 Characters, just seemed like a good number after we've stripped out stuff that isnt parseable
                yield [heading, clean_text(content)]
            content = ""
            heading = line
            continue

        if not heading:
            continue
        content += line

all_list_and_breaks  = re.compile(r"<li/?>|<br/?>|</li>|</p>|</a>", re.IGNORECASE)
all_headers = re.compile('^h[1-6]$')

def unique_everseen(iterable, key=None):
    "List unique elements, preserving order. Remember all elements ever seen."
    # unique_everseen('AAAABBBCCDAABBB') --> A B C D
    # unique_everseen('ABBCcAD', str.lower) --> A B C D
    seen = set()
    seen_add = seen.add
    if key is None:
        for element in ifilterfalse(seen.__contains__, iterable):
            seen_add(element)
            yield element
    else:
        for element in iterable:
            k = key(element)
            if k not in seen:
                seen_add(k)
                yield element

def unique_words(string, ignore_case=False):
    key = None
    if ignore_case:
        key = str.lower
    return " ".join(unique_everseen(string.split(), key=key))

def handle_code(x, soup):
    tag = soup.new_tag("code")
    tag.attrs = dict(x.attrs)
    tag.string = "'" + x.text.replace('\t', " ").replace("\n", "; ") + "'"
    x.replace_with(tag)

def handle_link(x, soup):
    tag = soup.new_tag("a")
    tag.attrs = dict(x.attrs)
    if 'href' in x.attrs:
        tag.string = tag.text + "@" + tag.attrs['href']
    else:
        tag.string = tag.text
    x.replace_with(tag)

def handle_tags(el, soup):
    if el.name == "code":
        return handle_code(el, soup)
    if el.name == "a":
        return handle_link(el, soup)
    if el.name == "script":
        el.extract()
        return

    tag = soup.new_tag(el.name)
    tag.attrs = dict(el.attrs)
    tag.string = " " + el.text.replace('\t', " ").replace('*', '') + " "
    el.replace_with(tag)

def clean_title(s):
    drop_formatting = s.replace('"', '').replace("'", '').replace('#','')
    return re.sub('^ +|\n', '', drop_formatting)

unacceptable_titles = ["handbook", "labor and employment notices"]
unacceptable_headers = ["quick links"]
def handbook_md(f, n):
    all_text = f.read()
    m = re.search('title: (.+)\n', all_text)
    mut_text = all_text
    if m:
        title = clean_title(m[0].split(':', 1)[1])
        rendered = mistletoe.markdown(all_text, HTMLRenderer)
        rendered = re.sub(all_list_and_breaks, ' ', rendered)
        rendered = rendered.replace('  ', ' ')
        soup = BeautifulSoup(rendered, 'html.parser')
        headers = [x.text for x in soup.find_all(all_headers)]
        [handle_tags(x, soup) for x in soup.find_all()]
        title = title.strip()

        ret = [(title, x[0].replace(title, "").strip(), x[1]) for x in sections_content(soup.get_text(separator="\n"), headers)]
        return list(filter(lambda x: (x[0].lower() not in unacceptable_titles and x[1].lower() not in unacceptable_headers), ret))
    return []

def runbook_md(f, n):
    if 'vendor/' in n:
        # nope.
        return []
    rendered = mistletoe.markdown(f.read(), HTMLRenderer)
    rendered = re.sub(all_list_and_breaks, ' ', rendered)
    rendered = rendered.replace('  ', ' ')
    soup = BeautifulSoup(rendered, 'html.parser')
    [handle_code(x, soup) for x in soup(['code'])]
    headers = [x.text for x in soup.find_all(all_headers)]
    [handle_tags(x, soup) for x in soup.find_all()]
    # sadly in runbook land, there's no pretty metadata on the documents
    # so we'll construct it from the path.
    # also remove all the duplicates, so we dont end up with 'git git or logging logging'
    title = ' '.join(n.split('/')[-2:])
    title = title.replace('.md', '').replace('-', ' ').replace('_', ' ').replace('uncategorized', '').title()
    if title == "Readme":
        title = "General"
    title = title.replace("Readme", "").strip()
    title = unique_words(title)
    title = title.strip()

    ret = [(title, x[0].replace(title, "").strip(), x[1]) for x in sections_content(soup.get_text(separator="\n"), headers)]
    return list(filter(lambda x: (x[0].lower() not in unacceptable_titles and x[1].lower() not in unacceptable_headers), ret))

paths_to_search = {
    "/home/calliope/src/gitlab.com/gitlab-com/www-gitlab-com/sites/handbook" : handbook_md,
    "/home/calliope/src/gitlab.com/gitlab-com/runbooks" : runbook_md,
}

def main():
    connection = sqlite3.connect("../data/embeddings.db", isolation_level=None)
    cursor = connection.cursor()
    cursor.execute("CREATE TABLE Data (id INTEGER PRIMARY KEY AUTOINCREMENT, Title TEXT, Heading TEXT, Content TEXT, Embedding TEXT)")

    for gp in paths_to_search:
        table_contents = []
        for p in glob.iglob("**/*.md", root_dir=gp, recursive=True):
            f = open(gp + "/" + p)
            print(gp + "/" + p)
            cursor.executemany("INSERT INTO Data(Title, Heading, Content) VALUES (?, ?, ?)", paths_to_search[gp](f, p))
    connection.commit()
    cursor.close()
main()
