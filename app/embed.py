from openai.embeddings_utils import get_embedding
import os
import time
import sqlite3
import ujson

EMBEDDING_MODEL = "text-embedding-ada-002"
EMBEDDING_ENCODING = "cl100k_base"
MAX_TOKENS = 8000

connection = sqlite3.connect("../data/embeddings.db", isolation_level=None)
cursor = connection.cursor()
cursor.execute("SELECT * FROM DATA")
rows = cursor.fetchall()

for row in rows:
    _id    = row[0]
    title  = row[1]
    heading = row[2]
    content = row[3]

    combined = f"Title: {title}; Heading: {heading}; Content: {content}"
    cursor.execute("UPDATE Data SET Embedding = ? WHERE id = ?;", (ujson.dumps(get_embedding(combined, engine=EMBEDDING_MODEL)), _id))

connection.commit()
cursor.close()
