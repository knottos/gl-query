
import openai
import sys
import os
from openai.embeddings_utils import get_embedding
import tiktoken
import hnbinding
import sqlite3
import ujson

EMBEDDING_MODEL = "text-embedding-ada-002"
EMBEDDING_ENCODING = "cl100k_base"
COMPLETIONS_MODEL = "text-davinci-003"
MAX_SECTION_LEN = 8000
SEPARATOR = "\n* "
ENCODING = "cl100k_base"  # encoding for text-embedding-ada-002

encoding = tiktoken.get_encoding(ENCODING)
separator_len = len(encoding.encode(SEPARATOR))

d  = os.path.abspath(os.path.dirname(__file__))
data_file = d + '/../data/embeddings.db'

COMPLETIONS_API_PARAMS = {
    # We use temperature of 0.0 because it gives the most predictable, factual answer.
    "temperature": 0.0,
    "max_tokens": 300,
    "model": COMPLETIONS_MODEL,
}

# With hypernonsense
def line_tuple(idx):
    connection = sqlite3.connect("../data/embeddings.db", isolation_level=None)
    cursor = connection.cursor()
    cursor.execute(f"SELECT Content FROM Data WHERE id = {idx};")
    return cursor.fetchall()[0][0]

def search_docs(index, user_query, top_n=20):
    embedding = get_embedding(
        user_query,
        engine=EMBEDDING_MODEL
    )
    res = [line_tuple(i) for i in index.nearest(embedding, top_n)]
    return res


def construct_prompt(question: str, index) -> str:
    """
    Fetch relevant 
    """
    most_relevant_document_sections = search_docs(index, question)
    
    chosen_sections = []
    chosen_sections_len = 0
    #chosen_sections_indexes = []
     
    for _, section_index in enumerate(most_relevant_document_sections):
        # Add contexts until we run out of space.        
        document_section = section_index
        
        chosen_sections_len += len(document_section) + separator_len
        if chosen_sections_len > MAX_SECTION_LEN:
            break
            
        chosen_sections.append(SEPARATOR + document_section.replace("\n", " "))
        #chosen_sections_indexes.append(str(section_index))
            
    
    print(chosen_sections)

    header = """Answer the question as truthfully as possible using the provided context, if the answer is not contained within the text below, say "I don't know."\n\nContext:\n"""
    return header + "".join(chosen_sections) + "\n\n Q: " + question + "\n A:"


def answer_query_with_context(
    query: str,
    index,
    show_prompt: bool = False
) -> str:
    prompt = construct_prompt(
        query,
        index
    )
    if show_prompt:
        print(prompt)

    response = openai.Completion.create(
                prompt=prompt,
                **COMPLETIONS_API_PARAMS
            )

    return response["choices"][0]["text"].strip(" \n")