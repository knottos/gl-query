{
  pkgs ? import (fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/e285dd0ca97.tar.gz";
    sha256 = "sha256:09kzsi7ivvq4cziyvar6bppmm6sx9i7w5b4ha2k94w6yns37nr0d";
  }) { config.allowUnfree = true; }
}:

pkgs.python3.pkgs.buildPythonPackage rec {
    pname = "hnbinding";
    version = "0.1.1";
    src = ./rust/hnbinding;
    cargoDeps = pkgs.rustPlatform.importCargoLock {
      lockFile = ./rust/hnbinding/Cargo.lock;
    };
    nativeBuildInputs = [ 
      pkgs.python3.pkgs.setuptools-rust
    ] ++ (with pkgs.rustPlatform; [
      rust.cargo
      rust.rustc
      cargoSetupHook
    ]);
    doCheck = false;
  }